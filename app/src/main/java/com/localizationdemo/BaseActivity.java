package com.localizationdemo;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.localizationdemo.utility.ContextWrapper;
import com.localizationdemo.utility.SharedPrefsForeverHelper;

import java.util.Locale;

/*
    Developed By : Javid Piprani

 */

public abstract class BaseActivity extends AppCompatActivity {


    @Override
    public void attachBaseContext(Context newBase) {
        String languageCode = SharedPrefsForeverHelper.getInstance().getLocalizeLanguage();
        if (TextUtils.isEmpty(languageCode)) {
            languageCode = Locale.getDefault().getLanguage();
            SharedPrefsForeverHelper.getInstance().setLocalizeLanguage(languageCode);
        }
        Context context = ContextWrapper.wrap(newBase, new Locale(languageCode));
        super.attachBaseContext(context);

    }


}
