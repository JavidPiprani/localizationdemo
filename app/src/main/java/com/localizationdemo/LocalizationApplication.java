package com.localizationdemo;

import android.app.Application;

/*
    Developed By : Javid Piprani

 */
public class LocalizationApplication extends Application {

    public static LocalizationApplication appInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        appInstance = this;
    }

    /**
     * @return instance of application class
     */
    public static synchronized LocalizationApplication getInstance() {
        return appInstance;
    }


}
